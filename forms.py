
class WizardForm:

    data = {}

    def __init__(self, *args, **kwargs):
        assert "browser" in kwargs
        self.browser = kwargs["browser"]
        for field, value in kwargs.items():
            self.data[field] = value

    @property
    def next_step_button(self):
        return self.browser.find_element_by_css_selector('li.wizard-next > a.wizard-nav')

    def next_step(self):
        self.next_step_button.click()


class NewContractStep1Form(WizardForm):
    _nationalities = {
        "spanish": "60"
    }

    _sexes = {
        "male": "1",
        "female": "2",
        "other": "3"
    }

    _min_year = {
        "id": 101,
        "year": 1920
    }

    _langs = {
        "ca": "1",
        "es": "2",
        "fr": "3",
        "en": "4",
    }

    _subdivisions = {
        "alicante": "1"
    }

    data = {
        "nationality": _nationalities["spanish"],
        "birthdate": "26/6/1980",
        "sex": _sexes["male"],
        "address": "Graner 10",
        "city": "LH",
        "zip": "08904",
        "lang": _langs["ca"],
        "subdivision": _subdivisions["alicante"],
    }

    @property
    def new_customer_button(self):
        return self.browser.find_element_by_css_selector('input#partner_do_option-0 + label > span')

    @property
    def dni_input(self):
        return self.browser.find_element_by_css_selector('input#vat_number')

    @property
    def name_input(self):
        return self.browser.find_element_by_css_selector('input#name')

    @property
    def surname_input(self):
        return self.browser.find_element_by_css_selector('input#surname')

    @property
    def lastname_input(self):
        return self.browser.find_element_by_css_selector('input#lastname')

    @property
    def nationality_button(self):
        return self.browser.find_element_by_css_selector('div#nationality_chosen > a')

    @property
    def nationality_button(self):
        return self.browser.find_element_by_css_selector('div#nationality_chosen')

    @property
    def birthdate_day_button(self):
        return self.browser.find_element_by_css_selector('div#birth_date_day_chosen')

    @property
    def birthdate_month_button(self):
        return self.browser.find_element_by_css_selector('div#birth_date_month_chosen')

    @property
    def birthdate_year_button(self):
        return self.browser.find_element_by_css_selector('div#birth_date_year_chosen')

    @property
    def sex_button(self):
        return self.browser.find_element_by_css_selector('div#sex_chosen')

    @property
    def address_input(self):
        return self.browser.find_element_by_css_selector('input#street')

    @property
    def city_input(self):
        return self.browser.find_element_by_css_selector('input#city')

    @property
    def phone_input(self):
        return self.browser.find_element_by_css_selector('input#phone')

    @property
    def subdivision_button(self):
        return self.browser.find_element_by_css_selector('div#subdivision_chosen')

    @property
    def lang_button(self):
        return self.browser.find_element_by_css_selector('div#lang_chosen')

    @property
    def zip_input(self):
        return self.browser.find_element_by_css_selector('input#zip')

    @property
    def email_input(self):
        return self.browser.find_element_by_css_selector('input#email')

    @property
    def discovery_channel_button(self):
        return self.browser.find_element_by_css_selector('div#discovery_channel_chosen')

    @property
    def next_step_button(self):
        return self.browser.find_element_by_css_selector('li.wizard-next > a.wizard-nav')

    def nationality_option(self, id):
        css_selector = "div#nationality_chosen > .chosen-drop > ul > li[data-option-array-index='{}']".format(id)
        return self.browser.find_element_by_css_selector(css_selector)

    def birthdate_day_option(self, id):
        css_selector = "div#birth_date_day_chosen > .chosen-drop > ul > li[data-option-array-index='{}']".format(id)
        return self.browser.find_element_by_css_selector(css_selector)

    def birthdate_month_option(self, id):
        css_selector = "div#birth_date_month_chosen > .chosen-drop > ul > li[data-option-array-index='{}']".format(id)
        return self.browser.find_element_by_css_selector(css_selector)

    def birthdate_year_option(self, id):
        css_selector = "div#birth_date_year_chosen > .chosen-drop > ul > li[data-option-array-index='{}']".format(id)
        return self.browser.find_element_by_css_selector(css_selector)

    def sex_option(self, id):
        css_selector = "div#sex_chosen > .chosen-drop > ul > li[data-option-array-index='{}']".format(id)
        return self.browser.find_element_by_css_selector(css_selector)

    def subdivision_option(self, id):
        css_selector = "div#subdivision_chosen > .chosen-drop > ul > li[data-option-array-index='{}']".format(id)
        return self.browser.find_element_by_css_selector(css_selector)

    def lang_option(self, id):
        css_selector = "div#lang_chosen > .chosen-drop > ul > li[data-option-array-index='{}']".format(id)
        return self.browser.find_element_by_css_selector(css_selector)

    def discovery_channel_option(self, id):
        css_selector = "div#discovery_channel_chosen > .chosen-drop > ul > li[data-option-array-index='{}']".format(id)
        return self.browser.find_element_by_css_selector(css_selector)

    def fill(self):
        if self.data["new_customer"]:
            self.new_customer_button.click()

        self.dni_input.send_keys(self.data["dni"])
        self.name_input.send_keys(self.data["name"])
        self.surname_input.send_keys(self.data["surname"])
        self.lastname_input.send_keys(self.data["lastname"])

        self.nationality_button.click()
        self.nationality_option(self.data["nationality"]).click()

        self._fill_birthdate()

        self.sex_button.click()
        self.sex_option(self.data["sex"]).click()

        self.address_input.send_keys(self.data["address"])
        self.city_input.send_keys(self.data["city"])
        self.zip_input.send_keys(self.data["zip"])

        # yes, this is a dirty trick -
        # we need to click twice in order to make this button work,
        # since JS is asynchronously loading the droddown menu items.
        self.subdivision_button.click()
        self.subdivision_button.click()
        self.subdivision_option(self.data["subdivision"]).click()

        self.phone_input.send_keys(self.data["phone"])

        self.lang_button.click()
        self.lang_option(self.data["lang"]).click()

        self.discovery_channel_button.click()
        self.discovery_channel_option("1").click()

        self.email_input.send_keys(self.data["email"])
    def _fill_birthdate(self):
        day, month, year = self.data["birthdate"].split("/")

        year_option_id = self._year_to_option_id(year)

        self.birthdate_day_button.click()
        self.birthdate_day_option(day).click()

        self.birthdate_month_button.click()
        self.birthdate_month_option(month).click()

        self.birthdate_year_button.click()
        self.birthdate_year_option(year_option_id).click()

    def _year_to_option_id(self, year):
        year_count = int(year) - self._min_year["year"]
        return self._min_year["id"] - year_count


class NewContractStep2Form(WizardForm):
    _services = {
        "mobile": 0,
        "adsl": 1,
        "mobile+adsl": 2,
        "membership": 3
    }

    _calls_mins = {
        "0min": 0,
        "100min": 1,
        "200min": 2,
        "300min": 3,
        "nolimit": 4,
    }

    _internet_mbs = {
        "0mb": 0,
        "200mb": 1,
        "500mb": 2,
        "1gb": 3,
        "2gb": 4,
    }

    _default_data = {
        "service": _services["mobile"],
    }

    def services_option(self, id):
        return self.browser.find_element_by_css_selector("label[for='service-{}']".format(id))

    def fill(self):
        raise NotImplementedError("Subclasses should implement this")


class NewMobileContractStep2Form(NewContractStep2Form):

    _default_data = {
        "service": "mobile",
        "calls_mins": "100min",
        "internet_mbs": "1gb"
    }

    @property
    def data(self):
        return {
            "service": self._services[self._default_data["service"]],
            "calls_mins": self._calls_mins[self._default_data["calls_mins"]],
            "internet_mbs": self._internet_mbs[self._default_data["internet_mbs"]]

        }

    def calls_mins_option(self, id):
        return self.browser.find_element_by_css_selector("div#switch-mobile_min-{} > span".format(id))

    def internet_mbs_option(self, id):
        return self.browser.find_element_by_css_selector("div#switch-mobile_internet-{} > span".format(id))

    def fill(self):
        self.services_option(self.data["service"]).click()
        self.calls_mins_option(self.data["calls_mins"]).click()
        self.internet_mbs_option(self.data["internet_mbs"]).click()


class NewADSLContractStep2Form(NewContractStep2Form):

    _default_data = {
        "service": "adsl",
    }

    @property
    def data(self):
        return {
            "service": self._services[self._default_data["service"]],
        }

    def fill(self):
        self.services_option(self.data["service"]).click()


class NewMobileContractStep3Form(WizardForm):
    _mobile_options = {
        "keep": 0,
        "new": 1
    }

    def mobile_option(self, id):
        return self.browser.find_element_by_css_selector("label[for='mobile_option-{}']".format(id))

    def fill(self):
        self.mobile_option(self._mobile_options["new"]).click()


class NewADSLContractStep3Form(WizardForm):

    _subdivisions = {
        "alicante": "1"
    }

    @property
    def name_input(self):
        return self.browser.find_element_by_css_selector('input#internet_name')

    @property
    def surname_input(self):
        return self.browser.find_element_by_css_selector('input#internet_surname')

    @property
    def lastname_input(self):
        return self.browser.find_element_by_css_selector('input#internet_lastname')

    @property
    def vat_number_input(self):
        return self.browser.find_element_by_css_selector('input#internet_vat_number')

    @property
    def street_input(self):
        return self.browser.find_element_by_css_selector('input#internet_street')

    @property
    def zip_input(self):
        return self.browser.find_element_by_css_selector('input#internet_zip')

    @property
    def city_input(self):
        return self.browser.find_element_by_css_selector('input#internet_city')

    @property
    def subdivision_button(self):
        return self.browser.find_element_by_css_selector('div#internet_subdivision_chosen')

    def subdivision_option(self, id):
        css_selector = "div#internet_subdivision_chosen > .chosen-drop > ul > li[data-option-array-index='{}']".format(id)
        return self.browser.find_element_by_css_selector(css_selector)

    def fill(self, **data):
        self.name_input.send_keys(data["name"])
        self.surname_input.send_keys(data["surname"])
        self.lastname_input.send_keys(data["lastname"])
        self.vat_number_input.send_keys(data["dni"])

        self.street_input.send_keys(data["address"])
        self.zip_input.send_keys(data["zip"])
        self.city_input.send_keys(data["city"])
        self.subdivision_button.click()
        self.subdivision_option(self._subdivisions["alicante"]).click()


class NewContractStep4Form(WizardForm):

    @property
    def iban_input(self):
        return self.browser.find_element_by_css_selector("input#bank_iban")

    @property
    def confirm_button(self):
        return self.browser.find_element_by_css_selector('li.wizard-confirm > a.wizard-nav')

    @property
    def agree_button(self):
        return self.browser.find_element_by_css_selector("label[for='agree'] > span")

    def fill(self):
        self.iban_input.send_keys(self.data["iban"])
        self.agree_button.click()

    def confirm(self):
        self.confirm_button.click()